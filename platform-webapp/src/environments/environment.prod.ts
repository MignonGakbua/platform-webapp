import { HttpHeaders } from '@angular/common/http';

export const environment = {
  production: true,
  apiURL:'',
  httpOptions : {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin':'*'
    })
   }
};